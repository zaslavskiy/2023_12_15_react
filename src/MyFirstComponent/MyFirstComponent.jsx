import "./MyFirstComponent.scss"

import React, {Component} from 'react';
import {logDOM} from "@testing-library/react";

class MyFirstComponent extends Component {

  // constructor(props) {
  //   super(props);
  // }

  render() {

    console.log(this.props);

    const style = {
      color: "red"
    }

    return (
      <>

        {this.props.color}

        {/*<p style={style} >Hello React!</p>*/}
        {/*<p style={{color:"red"}}>Hello React!</p>*/}

        <p style={{color: this.props.color}}>Hello React!</p>

        <p>Hello React!</p>
      </>
    );
  }
}

export default MyFirstComponent;