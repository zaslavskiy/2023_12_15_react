import React, {Component} from 'react';

class Counter extends Component {
  state={
    count:0
  }

  countClickHandler(){
    this.setState((state) => ({
      count: state.count + 1
    }));
    // console.log(this.state.count);

  }

  componentDidMount() {
    console.log("componentDidMount")
  }

  componentWillUnmount() {
    console.log("componentWillUnmount")

  }

  render() {
    // const qqq = "3"
    return (

      <div>
        <p>{this.props.name}</p>
        <p>{this.state.count}</p>
        <button onClick={()=>this.countClickHandler()}> Inc </button>
        {/*<button onClick={()=>this.setState({count:this.state.count+1})}> Inc </button>*/}

        {/*<button onClick={()=>this.setState((state) => ({*/}
        {/*  count: state.count + 1*/}
        {/*}))}> Inc </button>*/}

        {/*<button onClick={(e)=>this.countClickHandler(e,qqq)}>Click {this.state.count}</button>*/}
      </div>
    );
  }
}

export default Counter;