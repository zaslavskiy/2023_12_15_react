import logo from './logo.svg';
import './App.scss';
import MyFirstComponent from "./MyFirstComponent/MyFirstComponent";
import Counter from "./Counter/Counter";

function App() {
  return (
    <div className="App">


      <Counter name={"Counter1"}/>

      <MyFirstComponent color={"red"} size={16} />
      <MyFirstComponent color={"gray"} size={16}  />

      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
